# Objectif : 
Utiliser l'API Movie Database, afin de réaliser une interface utilisateur.

### Temps estimé / maximum : 1h

# Doc API : 
https://developers.themoviedb.org/3/getting-started/introduction

Votre rendu final devra comprendre la liste des fonctionnalités présentes ci-dessous. Libre à 
vous d'utiliser les bibliothèques, outils que vous voulez pour mener à bien votre 
développement.

### Affichage des derniers films :
Un utilisateur peut voir les derniers films sur la page d'accueil.
### Affichage des films dans l'ordre :
Un utilisateur peut afficher les films par leur ordre de sortie.
### Affichage d'un film sur une autre page :
Un utilisateur peut alors voir tous les détails d'un film (vous êtes libre d'afficher le 
nombre d'informations que vous voulez).
### Afficher les suggestions d'un film :
Une fois sur la page de détails, un utilisateur peut voir les suggestions alternatives en 
rapport avec ce film.
### Création d'une liste de film à voir :
Un utilisateur peut se créer une liste de film à voir.
### Ajout d'un système de rating :
Un utilisateur peut ajouter une note sur chaque film.


### Livrable
Ce que nous attendons:

- Votre implémentation en Typescript
- Fournissez nous un lien de votre repository avec votre implémentation
- Commit régulièrement avec des messages explicite
- A un fichier, un message, un email expliquant le process et vos principes que vous avez suivi pour effectuer ce kata. Expliquez comment un developper pourra réutiliser vos fonctionnalités dans le futur.

Bon courage !
